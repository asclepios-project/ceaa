#!/bin/sh
# docker-compose up elasticsearch &
# sleep 20
# docker-compose down


export $(egrep -v '^#' .env | xargs)

echo -e "
sudo curl -L git.io/weave -o /usr/local/bin/weave
sudo chmod a+x /usr/local/bin/weave


iptables -A INPUT -p tcp -m tcp --dport 6783 -j ACCEPT
iptables -A OUTPUT -p tcp -m tcp --sport 6783 -j ACCEPT


iptables -A INPUT -p udp -m udp --dport 6783:6784 -j ACCEPT
iptables -A OUTPUT -p udp -m udp --sport 6783:6784 -j ACCEPT

weave launch --password ${REMOTE_PASSWORD}


Please run the above commands in order to initialize the secure connection between yours server and ours. ( run only once or if there is any change in remote IP or password then run it again)

continue?...press any key"
read val

sed -i -e "s|xpack\.monitoring\.elasticsearch\.password\: .*|xpack\.monitoring\.elasticsearch\.password\: "${ELK_PASSWORD:-changeme}"|g" ./logstash/config/logstash.yml
sed -i -e "s|elasticsearch\.password\: .*|elasticsearch\.password\: "${ELK_PASSWORD:-changeme}"|g" ./kibana/config/kibana.yml
sed -i -e "s|password => .*|password => \""${ELK_PASSWORD:-changeme}"\"|g" ./logstash/pipeline/logstash.conf

eval $(weave env)
weave expose
docker-compose  -f docker-compose.yml -f extensions/logspout/logspout-compose.yml  up --build -d

#sed -i -e "s|xpack\.monitoring\.elasticsearch\.password\: .*|xpack\.monitoring\.elasticsearch\.password\: xxxxxxxxxxxxxxxxxxxxxx|g" ./logstash/config/logstash.yml
#sed -i -e "s|elasticsearch\.password\: .*|elasticsearch\.password\: xxxxxxxxxxxxxxxxxxxxxx|g" ./kibana/config/kibana.yml
#sed -i -e "s|password => \".*\"|password => \"xxxxxxxxxxxxxxxxxxxxxx\"|g" ./logstash/pipeline/logstash.conf
