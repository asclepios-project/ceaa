#!/bin/bash -x


export $(egrep -v '^#' .env | xargs)

ls /usr/local/bin/weave
if [ "$?" = "1"]; 
then
    sudo curl -L git.io/weave -o /usr/local/bin/weave
    sudo chmod a+x /usr/local/bin/weave
fi

[ -z "$REMOTE_SERVER" ] && echo "Empty REMOTE_SERVER please check the .env file" && exit 1
[ -z "$REMOTE_PASSWORD" ] && echo "Empty REMOTE_PASSWORD please check the .env file" && exit 1

iptables -A INPUT -s ${REMOTE_SERVER} -p tcp -m tcp --dport 6783 -j ACCEPT
iptables -A OUTPUT -d ${REMOTE_SERVER} -p tcp -m tcp --sport 6783 -j ACCEPT

iptables -A INPUT  -s ${REMOTE_SERVER} -p udp -m udp --dport 6783:6784 -j ACCEPT
iptables -A OUTPUT  -d ${REMOTE_SERVER} -p udp -m udp --sport 6783:6784 -j ACCEPT

weave launch --password ${REMOTE_PASSWORD} ${REMOTE_SERVER}


eval $(weave env)
export LOGSTASHIP=`weave status dns | grep ceaa_logstash | awk 'NR==1{print $2}'`
echo "LOGSTASHIP=${LOGSTASHIP}"
weave expose
docker-compose up --build -d 
