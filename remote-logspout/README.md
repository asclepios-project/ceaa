# Prerequisites

- linux server
- Docker
- Docker-compose
- weave (https://www.weave.works/docs/net/latest/install/installing-weave/)
- .env file

# Run

```sh
bash ./run.sh
```

# Log snifing

## Preferable log template 
You should print on stdout/stderr from an app in a container, what ever you want to sniffed from ceaa using the below template.
```
{ 
        "type": "response",
        "@timestamp": "2021-11-30T09:09:02+00:00",
        "method": <request.method>,
        "statusCode": <http_code>,
        "req": {
            "url": <request.url>,
            "method": <request.method>,
                "headers": {
                "host": <request.host>,
                "user-agent": <str(request.user_agent)>,
                "accept": <str(request.accept_charsets)>,
                "accept-language": <str(request.accept_languages)>,
                "accept-encoding": <str(request.accept_encodings)>,
                "referer": <str(request.referrer)>,
                "content-type": <str(request.content_type)>,
                "origin": <str(request.origin)>,
                "content-length": <str(request.content_length)>,
                "connection": <str(request.headers.get("connection"))>,
                "remote_addr": <str(request.remote_addr)>,
                "remote_user": <str(request.remote_user)>,
            },
        },
        "res": { 
            "statusCode": <http_code>, 
            "contentLength": <resp.content_length>
        },
        "message": <str(message)>
}
```

## Exclude containers
if you need to exclude a container from the sniffing you should add in docker-compose (supported version 3.7) the below field:

```yaml
    labels:
      - "logspout.exclude=true"
```
